class ExampleCommand : SubCommand {
	// Defines option parsing for the subcommand
	private static bool testbool;
	private const OptionEntry[] _options = {
		{ "test-bool", 0, 0, OptionArg.NONE, ref testbool, "Testing subcommand parameters", null },
		{ null }
	};

	// Configure the subcommand
	construct {
		name = "test";
		desc = "Test how subcommands work";
		params = "[--test-bool]";
		options = _options;
		//subcommands = {
		//	new ExampleSubCommand ()
		//};
		//expected_args = 1;
	}

	// Entry point into the subcommand
	public override int main (string[] args) {
		print ("HELLO WORLD - THIS IS TEST!\n");
		if (testbool) {
			print ("TEST BOOL!!!!\n");
			return 0;
		}
		return 0; //-1;
	}

	protected override bool check_enough_args () {
		return testbool; // If we have testbool, there are enough args
	}
}

class ExampleSubCommand : SubCommand {
	construct {
		name = "subcommand";
		desc = "Sub command in a subcommand";
	}

	public override int main (string[] args) {
		return 0;
	}
}