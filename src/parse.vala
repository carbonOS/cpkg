using Cli;

abstract class SubCommand : GLib.Object {
	public static bool script = false;

	public string name { get; protected set; default = "UNSET"; }
	public string desc { get; protected set; default = ""; }
	public string params { get; protected set; default = "[OPTIONS]"; }
	public OptionEntry[]? options { get; protected set; default = null;  }
	public SubCommand[]? subcommands { get; protected set; default = null;  }

	protected int help_length { get; set; default = 18; } // The offset at which to print desc in help
	protected int expected_args { get; set; default = 0; } // How many non-flag args are required

	public int invoke (string[] args) {
		try {
			var arg0 = build_valid_arg0 (args, false, true);
			var parser = new OptionContext (@"$arg0$params - $desc");
			parser.set_help_enabled (!script);
			if (options != null)
				parser.add_main_entries (options, null);
			
			// Deal with subcommands
			if (subcommands != null && subcommands.length > 0) {
				// NOTE: This line imposes a limitation on the behavior of subcommands.
				// VALID: cpkg OPTIONS subcommand OPTIONS (only one level of subcommand)
				// VALID: cpkg OPTIONS subcommand subcommand OPTIONS (no options in between subcommands)
				// INVALID: cpkg OPTIONS subcommand OPTIONS subcommand OPTIONS (options in between subcommands = illegal)
				// TODO: Figure out a solution to this.
				parser.set_strict_posix (true);

				// Generate special help section
				string commands = "Available commands:\n";
				foreach (var subcommand in subcommands) {
					int spaces = help_length - subcommand.name.length;
					commands += @"  $(subcommand.name)";
					for (int i = 0; i < spaces; i++) commands += " ";
					commands += @"$(subcommand.desc)\n";
				}
				parser.set_description (commands);
			}

			parser.parse (ref args);
		} catch (OptionError e) {
			return print_opt_error (e.message, args);
		}

		if (args.length < (expected_args + 1) && !check_enough_args ()) 
			return not_enough_arguments_error (args);
			
		int main_status = main (args); // Invoke the main command
		
		if (main_status == -1) { // Subcommands take control if main command returns -1
			foreach (var subcommand in subcommands) {
				if (args[expected_args] == subcommand.name) {
					// Some extra setup of the subcommand
					subcommand.expected_args += this.expected_args; // Subcommands require at least as many arguments as is required to run them.
					subcommand.script = this.script;
					
					return subcommand.invoke (args.copy ());
				}
			}
			return print_opt_error (@"Invalid subcommand %s".printf (args[expected_args]), args);
		}
		return main_status;
	}

	private string build_valid_arg0 (string[] args, bool include_original = true, bool space = false) {
		string output = "";
		if (include_original) output += args[0] + " ";
		for (int i = 1; i < expected_args; i++) {
			var arg = args[i];
			if (arg != null) output += @"$(args[i]) ";
		}
		if (!space) output = output.chomp ();
		return output;
	}

	protected int print_opt_error (string message, string[] args) {
		if (!script) {
			stderr.printf (@"$(RED + BOLD)ERROR: %s$(RESET)\n", message);
			print (@"Run $(BOLD)%s --help$(RESET) to see a full list of available command line options.\n", build_valid_arg0 (args));
		}
		return 1;
	}

	protected int not_enough_arguments_error (string[] args) {
		return print_opt_error ("Not enough arguments", args);
	}

	public abstract int main (string[] args); // Entry point for each subcommand

	protected virtual bool check_enough_args () {
		return false;
	}
}